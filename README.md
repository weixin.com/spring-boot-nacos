# springBoot-nacos

## 介绍
springBoot 结合 nacos 的配置中心 ，可以在线动态刷新配置

## nacos
Nacos可以从官网下载，单独的一个zip，解压即可运行 ，需要配置java环境变量 。
![输入图片说明](image-nacos.png)
在地址栏中进入控制台 ， 创建一个配置，example。 对应下面的Springboot启动类注解属性

## 项目结构
![输入图片说明](image.png)
首先是Maven父子项目，把对应的Pom文件放在对应的项目即可 ，consumer参照provider的项目搭建，很简单

## 项目代码
### test-provider项目的Application类
```java
package com.wesley.common;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.alibaba.nacos.spring.context.annotation.discovery.EnableNacosDiscovery;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author wwei5
 */
@SpringBootApplication
@NacosPropertySource(dataId = "example", autoRefreshed = true)
public class ProviderApp {

  public static void main(String[] args) {
    SpringApplication.run(ProviderApp.class, args);
    System.out.println("provider start ok ... ");
  }
}
```
### test-provider项目的Controller类
```java
package com.wesley.common.provider.controller;

import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.annotation.NacosValue;
import com.alibaba.nacos.api.exception.NacosException;
import com.alibaba.nacos.api.naming.NamingService;
import com.alibaba.nacos.api.naming.pojo.Instance;
import java.util.List;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * <h1> 类描述 </h1>
 *
 * @author Wesley.Wei
 * @date 2024-04-24 4:54 下午
 */
@RestController
@RequestMapping("/provider")
public class ProviderController {

  // 动态获取nacos的配置
  @NacosValue(value = "${name:李四}", autoRefreshed = true)
  private String localName;

  @GetMapping("/hello")
  public String hello() {
    return "hello " + localName;
  }

}
```

 
